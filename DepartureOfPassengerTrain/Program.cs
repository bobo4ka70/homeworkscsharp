﻿using System;
using System.Collections.Generic;
    //
    // Задача:
    // У вас есть программа, которая помогает пользователю составить план поезда.
    // Есть 4 основных шага в создании плана:
    // -Создать направление - создает направление для поезда(к примеру Бийск - Барнаул)
    // -Продать билеты - вы получаете рандомное кол-во пассажиров, которые купили билеты на это направление
    // -Сформировать поезд - вы создаете поезд и добавляете ему столько вагонов(вагоны могут быть разные по вместительности), сколько хватит для перевозки всех пассажиров.
    // -Отправить поезд - вы отправляете поезд, после чего можете снова создать направление.
    // В верхней части программы должна выводиться полная информация о текущем рейсе или его отсутствии.

namespace DepartureOfPassengerTrain
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            TrainStation trainStation = new TrainStation();
            trainStation.Work();
        }

        class TrainStation
        {
            private Train _train;
            
            private bool _isWork = true;
            
            private Random _random = new Random();
            public int Passengers { get; private set; }

            private void AddRandomNumberOfPassengers()
            {
                Passengers = _random.Next(60, 810);
            }

            private Train CreateTrain(string route)
            { 
                var newTrain = new Train(route);
                return newTrain;
            }

            private void SendCurrentTrainFromStation()
            {
                _train = null;
                Passengers = 0;
            }

            private void FormWagonTrain()
            {
                while (_train.CalculateTrainCapacity() < Passengers)
                {
                    _train.AddCarriage(_random);
                }
            }

            private bool TheTrainIsCreated()
            {
                if (_train == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }

            private void SendReportTrainNotCreated()
            {
                Console.WriteLine("Маршрут не создан, сначало создайте маршрут\n");
            }

            public void Work()
            {
                while (_isWork)
                {
                    Console.WriteLine("Железнодорожная станция Томска\n ");
                    Console.WriteLine("Текущий рейс:\n ");

                    if (TheTrainIsCreated())
                    {
                        Console.WriteLine(_train.Route + "\n ");
                    }
                    else
                    {
                        Console.WriteLine("Информация по текущему рейсу отсутствует\n ");
                    }

                    if (Passengers > 0)
                    {
                        Console.WriteLine($"Количество пассажиров, купивших билеты на рейс: {Passengers}\n ");
                    }

                    if (TheTrainIsCreated())
                    {
                        if (Passengers != 0 && _train.CalculateTrainCapacity() >= Passengers)
                        {
                            Console.WriteLine($"Количество мест для пассажиров: {_train.CalculateTrainCapacity()}");
                            Console.WriteLine($"Плацкартных вагонов: {_train.CalculateEconomClassCarriages()}");
                            Console.WriteLine($"Вагонов-купе: {_train.CalculateCompartmentCarriages()}");
                            Console.WriteLine("Поезд готов к отправке\n");
                        }
                    }

                    Console.WriteLine("Меню:\n ");
                    Console.WriteLine("Введите" +
                                      "\n1 - Создать направление" +
                                      "\n2 - Продать билеты" +
                                      "\n3 - Сформировать поезд" +
                                      "\n4 - Отправить поезд" +
                                      "\n5 - Завершить работу станции\n ");
                    Console.Write("Ввод: ");
                    string menuItem = Console.ReadLine();

                    while (menuItem != "1" && menuItem != "2" && menuItem != "3" && menuItem != "4" && menuItem != "5")
                    {
                        Console.Write("\nТакого пунка меню нет, введите номер ещё раз: ");
                        menuItem = Console.ReadLine();
                    }

                    switch (menuItem)
                    {
                        case "1":
                            if (TheTrainIsCreated())
                            {
                                Console.WriteLine("Сначала отправьте со станции текущий поезд\n ");
                            }
                            else
                            {
                                Console.Write("Введите название пункта прибытия Томск - ");
                                string endPoint = Console.ReadLine();

                                _train = CreateTrain($"Томск - {endPoint}");
                            }
                            break;
                        case "2":
                            if (TheTrainIsCreated())
                            {
                                if (Passengers == 0)
                                {
                                    AddRandomNumberOfPassengers();
                                }
                                else
                                {
                                    Console.WriteLine("Билеты уже раскуплены\n ");
                                }
                            }
                            else
                            {
                                SendReportTrainNotCreated();
                            }
                            break;
                        case "3":
                            if (TheTrainIsCreated())
                            {
                                if (_train.CalculateTrainCapacity() >= Passengers)
                                {
                                    Console.WriteLine("Поезд уже сформирован, согласно количеству купленных билетов\n ");
                                }
                                else
                                {
                                    FormWagonTrain(); 
                                }
                            }
                            else
                            {
                                SendReportTrainNotCreated();
                            }
                            break;
                        case "4":
                            if (TheTrainIsCreated())
                            {
                                if (Passengers == 0)
                                {
                                    Console.WriteLine("Чтобы отправить поезд, продайте билеты\n ");
                                }
                                else if (_train.CalculateTrainCapacity() < Passengers)
                                {
                                    Console.WriteLine("Чтобы отправить поезд, сформируйте его");
                                }
                                else
                                {
                                    SendCurrentTrainFromStation();
                                }
                            }
                            else
                            {
                                SendReportTrainNotCreated();
                            }
                            break;
                        case "5":
                            _isWork = false;
                            break;
                    }

                    Console.Write("\nКоманда выполнена, нажмите любую кнопку, чтобы обновить состояние рейса");
                    Console.ReadKey();
                    Console.Clear();
                }
            }
        }

        class Train
        {
            private List<Сarriage> _carriages  = new List<Сarriage>();

            public string Route { get; private set; }

            public void AddCarriage(Random random)
            {
                _carriages.Add(new Сarriage(random));
            }

            public int CalculateTrainCapacity()
            {
                int trainCapacity = 0;
                for (int i = 0; i < _carriages.Count; i++)
                {
                    trainCapacity += _carriages[i].Capacity;
                }

                return trainCapacity;
            }

            public int CalculateEconomClassCarriages()
            {
                int carriages = 0;

                for (int i = 0; i < _carriages.Count; i++)
                {
                    if (_carriages[i].Capacity == 36)
                    {
                        carriages++;
                    }
                }

                return carriages;
            }
            
            public int CalculateCompartmentCarriages()
            {
                int carriages = 0;

                for (int i = 0; i < _carriages.Count; i++)
                {
                    if (_carriages[i].Capacity == 54)
                    {
                        carriages++;
                    }
                }

                return carriages;
            }

            public Train(string route)
            {
                Route = route;
            }
        }

        class Сarriage
        {
            public Сarriage(Random random)
            {
                SetRandomCapacity(random);
            }
            
            public int Capacity { get; private set; }

            private void SetRandomCapacity(Random random) // Вагон купе - 36 человек, плацкарт 54, рандомно виберется один из видов
            {
                int[] twoNumbers = new[] {1, 2};

                int randomNubmer = twoNumbers[random.Next(twoNumbers.Length)];
                
                if (randomNubmer == 1)
                {
                    Capacity = 36;
                }
                else if (randomNubmer == 2)
                {
                    Capacity = 54;
                }
            }
        }
    }
}