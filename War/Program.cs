﻿using System;
using System.Collections.Generic;

    // Задача:
    // Есть 2 взвода. 1 взвод страны один, 2 взвод страны 2.
    // Каждый взвод внутри имеет солдат.
    // Нужно написать программу которая будет моделировать бой этих взводов.
    // Каждый боец - это уникальная единица, он может иметь уникальные способности или же уникальные характеристики, такие как повышенная сила.
    // Побеждает та страна, во взводе которой остались выжившие бойцы.
    // Не важно какой будет бой, рукопашный, стрелковый.

namespace War
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Battlefield battlefield = new Battlefield(); 
            battlefield.StartFight();
        }

        private static Random Random = new Random();

        class Battlefield
        {
            private bool _isFight = true;

            private Soldier _leftSideSoldier;
            private Soldier _rightSideSoldier;

            private Platoon _leftCountry = new Platoon();
            private Platoon _rightCountry = new Platoon();
            
            private void BeatsFirst(string fate)
            {
                switch (fate)
                {
                    case "left":
                        _leftSideSoldier.Battle(_rightSideSoldier);
                        if (_rightSideSoldier.Health == 0)
                        {
                            break;
                        }
                        else
                        {
                            _rightSideSoldier.Battle(_leftSideSoldier);
                            break;
                        }
                    case "right":
                        _rightSideSoldier.Battle(_leftSideSoldier);
                        if (_leftSideSoldier.Health == 0)
                        {
                            break;
                        }
                        else
                        {
                            _leftSideSoldier.Battle(_rightSideSoldier);
                            break;
                        }
                }
            }

            private void CheckStatusOfTheSoldiers()
            {
                if (_leftSideSoldier.Health == 0)
                {
                    Console.WriteLine("Сегодня боец левой стороны погиб");
                    _leftSideSoldier = _leftCountry.LaunchSoldierIntoBattle();
                    Console.WriteLine("\nНажми любую кнопку, для завершения дня");
                    Console.ReadKey();
                }
                else if (_rightSideSoldier.Health == 0)
                {
                    Console.WriteLine("Сегодня боец правой стороны погиб");
                    _rightSideSoldier = _rightCountry.LaunchSoldierIntoBattle();
                    Console.WriteLine("\nНажми любую кнопку, для завершения дня");
                    Console.ReadKey();
                }
                else
                {
                    Console.WriteLine("Сегодня в результате схватки между бойцами оба получили ранения и отступили");
                    Console.WriteLine("\nНажми любую кнопку, для завершения дня");
                    Console.ReadKey();
                }
            }

            private void CheckStatusOfThePlatoons()
            {
                if (_leftCountry.ShowSoldiersCount() == 0 && _rightCountry.ShowSoldiersCount() == 0)
                {
                    Console.Clear();
                    Console.WriteLine("Ничьи быть не должно...");
                    Console.WriteLine("Что-то пошло не так, проверь код, чтобы выйти нажми любую клавишу");
                    Console.ReadKey();
                    _isFight = false;
                }
                else if (_leftCountry.ShowSoldiersCount() == 0)
                {
                    Console.Clear();
                    Console.WriteLine("Страна справа одержала победу!");
                    Console.WriteLine("Нажмите любую клавишу, чтобы выйти");
                    Console.ReadKey();
                    _isFight = false;
                }
                else if (_rightCountry.ShowSoldiersCount() == 0)
                {
                    Console.Clear();
                    Console.WriteLine("Страна слева одержала победу!");
                    Console.WriteLine("Нажмите любую клавишу, чтобы выйти");
                    Console.ReadKey();
                    _isFight = false;
                }
            }

            private string TossTheLot()
            {
                string[] lots = "left|right".Split('|');
                return lots[Random.Next(lots.Length)];
            }

            public void StartFight()
            {
                Console.WriteLine("Две страны устроили между собой войну.");
                Console.WriteLine(
                    "С каждой стороны осталось по одному взводу, которые сойдутся в последней битве за победу.");
                Console.WriteLine("Нам же остаётся каждый день наблюдать за исходом очередного сражения между ними...\n ");
                
                _leftSideSoldier = _leftCountry.LaunchSoldierIntoBattle();
                _rightSideSoldier = _rightCountry.LaunchSoldierIntoBattle();

                while (_isFight)
                {
                    Console.WriteLine("Наступил новый день...\n ");

                    BeatsFirst(fate:TossTheLot());

                    CheckStatusOfTheSoldiers();
                    
                    Console.Clear();
                    
                    CheckStatusOfThePlatoons();
                }
            }
        }

        class Platoon
        {
            private Queue<Soldier> _platoon = new Queue<Soldier>();

            private void AddSoldiers(int platoonSize)
            {
                for (int i = 0; i < platoonSize; i++)
                {
                    _platoon.Enqueue(new Soldier());
                }
            }

            public Soldier LaunchSoldierIntoBattle()
            {
                return _platoon.Dequeue();
            }

            public int ShowSoldiersCount()
            {
                return _platoon.Count;
            }
            
            public Platoon()
            {
                AddSoldiers(platoonSize: 10);
            }
        }

        class Soldier
        {
            public int Health { get; private set; }
            public int Armor { get; private set; }
            public int Damage { get; private set; }

            private void SetRandomStats(int minHeath, int maxHeath, int minArmor, int maxArmor, int minDamage,
                int maxDamage)
            {
                if (minHeath < 1 || minHeath > maxHeath)
                {
                    Console.WriteLine("Некорректные показатели здоровья, проверьте вводные данные");
                    Console.WriteLine("Приложение будет закрыто, нажмите любую кнопку");
                    Console.ReadKey();
                    Environment.Exit(0);
                }
                else if (minArmor < 0 || minArmor > maxArmor)
                {
                    Console.WriteLine("Некорректные показатели брони, проверьте вводные данные");
                    Console.WriteLine("Приложение будет закрыто, нажмите любую кнопку");
                    Console.ReadKey();
                    Environment.Exit(0);
                }
                else if (minDamage < 1 || minDamage > maxDamage)
                {
                    Console.WriteLine("Некорректные показатели урона, проверьте вводные данные");
                    Console.WriteLine("Приложение будет закрыто, нажмите любую кнопку");
                    Console.ReadKey();
                    Environment.Exit(0);
                }
                else
                {
                    Health = Random.Next(minHeath, maxHeath);
                    Armor = Random.Next(minArmor, maxArmor);
                    Damage = Random.Next(minDamage, maxDamage);
                }
            }

            public void Battle(Soldier enemy)
            {
                if (enemy.Health <= (Damage - enemy.Armor))
                {
                    enemy.Health = 0;
                }
                else
                {
                    enemy.Health -= (Damage - enemy.Armor);
                }
            }

            public Soldier()
            {
                SetRandomStats(minHeath: 30, maxHeath: 50, minArmor: 0, maxArmor: 10, minDamage: 30, maxDamage: 50);
            }
        }
    }
}