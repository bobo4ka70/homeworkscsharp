﻿using System;
using System.Collections.Generic;

    // Задача:
    // Реализовать базу данных игроков и методы для работы с ней.
    // У игрока может быть порядковый номер, ник, уровень, флаг – забанен ли он(флаг - bool).
    // Реализовать возможность добавления игрока, бана игрока по порядковому номеру, разбана игрока по порядковому номеру и удаление игрока.
    //
    // ВАЖНОЕ ОТ ВОВЫ: ПРАВИЛЬНО ЛИ, ЧТО БАНЮ Я ЧЕРЕЗ БАЗУ, А НЕ ЧЕРЕЗ ИГРОКА, ВЕДЬ ЭТО ЛОГИЧНО? ИГРОК НЕ ДОЛЖЕН БАНИТЬ ЧЕРЕЗ СЕБЯ, ЭТО ДОЛЖНА ДЕЛАТЬ БАЗА?
    // НО ТОГДА СВОЙТВО IsBanned ДОСТУПНО ИЗ ВНЕШНЕГО КОДА, В ЭТОМ СОБСВЕННО И СУТЬ ВОПРОСА:
    // СДЕЛАТЬ КАК ЛОГИЧНО? ИЛИ ЗАЩИТИТЬ ОБЪЕКТ ОТ ВНЕШНЕГО КОДА?

namespace Home1
{
    internal class Program
    {
        public static void Main(string[] args)
        {
           DatabasePlayers databasePlayers = new DatabasePlayers();
           databasePlayers.Work();
        }
    }

    class Player
    {
        public Player(string nickName, int lvl, bool isBanned = false)
        {
            _nickName = nickName;
            _lvl = lvl;
            IsBanned = isBanned;
        }

        private string _nickName;
        private int _lvl;
        
        public bool IsBanned { get; set; }

        public string ShowStatus()
        {
            if (IsBanned)
            { 
                return $"{_nickName} его уровень = {_lvl}, он забанен";
            }
            else
            { 
                return $"{_nickName} его уровень = {_lvl}, он не забанен";
            }
        }
    }

    class DatabasePlayers
    {
        private bool _isWork = true;
        private List<Player> _playerList = new List<Player>();

        private void AddNewPlayer()
        {
            Console.Clear();
            Console.WriteLine("Вы перешли в меню добавления игрока");
            
            Console.Write("Введите никнейм игрока: ");
            string nickName = Console.ReadLine();

            Console.Write("\nВведите уровень игрока: ");
            string input = Console.ReadLine();
            int lvl;

            bool canParce = Int32.TryParse(input, out lvl);

            while ( ! canParce || lvl < 1)
            {
                Console.Write("\nНекорректный ввод, введите целое число больше 0: ");
                input = Console.ReadLine();
                
                canParce = Int32.TryParse(input, out lvl);
            }
            
            _playerList.Add(new Player(nickName, lvl));

            Console.WriteLine("\nИгрок добавлен! Нажмите любую кнопку, чтобы выйти в главное меню");
            Console.ReadKey();
            Console.Clear();
        }

        private void SetBanStatus(bool banStatus)
        {
            if (_playerList.Count == 0)
            {
               ReturnIsListNull();
            }
            else
            {
                if (banStatus)
                {
                    Console.Write($"\nВведите порядковый номер игрока, которого хотите забанить: ");
                }
                else
                {
                    Console.Write($"\nВведите порядковый номер игрока, которого хотите разбанить: ");
                }
            
                int index = InputAndCheckExistenceInList();
            
                _playerList[index].IsBanned = banStatus;

                if (banStatus)
                {
                    Console.WriteLine($"Игрок под номером {index + 1} забанен, нажмите любую кнопку, чтобы обновить список");
                }
                else
                {
                    Console.WriteLine($"Игрок под номером {index + 1} разбанен, нажмите любую кнопку, чтобы обновить список");
                }
            
                Console.ReadKey();
                Console.Clear();
            }
            
        }

        private void RemovePlayer()
        {
            if (_playerList.Count == 0)
            {
               ReturnIsListNull();
            }
            else
            {
                Console.Write("\nВведите порядковый номер игрока, которого хотите удалить: ");
            
                int index = InputAndCheckExistenceInList();
            
                _playerList.RemoveAt(index);
            
                Console.WriteLine($"Игрок под номером {index + 1} удалён, нажмите любую кнопку, чтобы обновить список");
            
                Console.ReadKey();
                Console.Clear();
            }
        }
        
        private int InputAndCheckExistenceInList()
        {
            string input = Console.ReadLine();
            int index;

            bool canParse = Int32.TryParse(input, out index);
            
            while ( ! canParse || (index - 1) < 0 || index > _playerList.Count)
            {
                Console.Write("\nТакого числа в списке нет, попробуйте снова: ");
                input = Console.ReadLine();
                
                canParse = Int32.TryParse(input, out index);
            }

            return --index; //в листе начинается с 0, в базе с 1, вводится номер из базы, поэтому -1
        }

        private void ReturnIsListNull()
        {
            Console.WriteLine("Список пуст, нажмите любую кнопку чтобы перети к выбору пункта меню");
            Console.ReadKey();
            Console.Clear();
        }

        public void Work()
        {
            while (_isWork)
            {
                Console.WriteLine("БАЗА ДАННЫХ ИГРОКОВ");

                if (_playerList.Count == 0)
                {
                    Console.WriteLine("\nСписок игроков пуст");
                }
                else
                {
                    for (int i = 0; i < _playerList.Count; i++)
                    {
                        Console.Write($"\n{i + 1} - {_playerList[i].ShowStatus()}");
                    } 
                }
                
                Console.WriteLine("\n \nМеню:");
                Console.WriteLine("1 - Добавить игрока\n2 - Забанить игрока по номеру\n" +
                                  "3 - Разбанить игрока по номеру\n4 - Удалить игрока по номеру\n5 - Завершить работу");
                Console.Write("Введите число: ");
                string menuItem = Console.ReadLine();

                while (menuItem != "1" && menuItem != "2" && menuItem != "3" && menuItem != "4" && menuItem != "5")
                {
                    Console.Write("\nТакого пунка меню нет, введите номер ещё раз: ");
                    menuItem = Console.ReadLine();
                }

                switch (menuItem)
                {
                    case "1":
                        AddNewPlayer();
                        break;
                    case "2":
                        SetBanStatus(true);
                        break;
                    case "3":
                        SetBanStatus(false);
                        break;
                    case "4":
                        RemovePlayer();
                        break;
                    case "5":
                        _isWork = false;
                        break;
                }
            }
        }
    }
}