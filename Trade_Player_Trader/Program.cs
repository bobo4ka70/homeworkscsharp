﻿using System;
using System.Collections.Generic;

// Задача:
// Существует продавец, он имеет у себя список товаров и при нужде может вам его показать, также продавец может 
// продать вам товар. После продажи товар переходит к вам, и вы можете также посмотреть свои вещи.
// Возможные классы – игрок, продавец, товар.
// Вы можете сделать так, как вы видите это.

namespace Trade_Player_Trader
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Shop shop = new Shop();
            shop.Work();
        }
    }

    class Item
    {
        public Item(string name, int price, int count)
        {
            if (price < 0)
            {
                Console.WriteLine("Ошибка при создании класса Item, введено отрицательное значение Price");
                Console.WriteLine("Нажмите любую кнопку, программа будет закрыта");
                Console.ReadKey();
                Environment.Exit(0);
            }
            else if (count < 1)
            {
                Console.WriteLine("Ошибка при создании класса Item, введено значение Count < 1");
                Console.WriteLine("Нажмите любую кнопку, программа будет закрыта");
                Console.ReadKey();
                Environment.Exit(0);
            }
            else
            {
                Name = name;
                Price = price;
                Count = count;
            }
        }

        public string Name { get; private set; }
        public int Price { get; private set; }
        public int Count { get; private set; }

        public void ReduceCount(int count)
        {
            if (count < 1)
            {
                Console.WriteLine("Колличество удаляемых преметов передано не верно");
            }
            else if (count > Count)
            {
                Console.WriteLine("Колличество удаляемых преметов передано не верно");
            }
            else
            {
                Count -= count;
            }
        }
    }

    class Trader
    {
        public List<Item> ItemsTrader { get; private set; }

        public Trader(List<Item> itemsTrader)
        {
            ItemsTrader = itemsTrader;
        }

        public void ShowItems()
        {
            for (int i = 0; i < ItemsTrader.Count; i++)
            {
                Console.WriteLine(
                    $"{i + 1} - {ItemsTrader[i].Name}, цена {ItemsTrader[i].Price}, кол-во {ItemsTrader[i].Count}");
            }
        }

        public void SellItems(Player player, int itemNumber, int countToPay = 1)
        {
            int priceToPay = ItemsTrader[itemNumber].Price * countToPay;

            bool canSell = player.GiveMoneyToPay(priceToPay);

            if (canSell)
            {
                player.AddItem(ItemsTrader[itemNumber]);

                if (ItemsTrader[itemNumber].Count == countToPay)
                {
                    ItemsTrader.RemoveAt(itemNumber);
                }
                else
                {
                    ItemsTrader[itemNumber].ReduceCount(countToPay);
                }

                Console.WriteLine("\nСделка прошла успешно");
            }
        }
    }

    class Player
    {
        public Player(int money)
        {
            if (money < 0)
            {
                Console.WriteLine("Ошибка при создании класса Player, введно отрицательное значение Money");
                Console.WriteLine("Нажмите любую кнопку, приложение будет закрыто");
                Console.ReadKey();
                Environment.Exit(0);
            }
            else
            {
                Money = money;
            }
        }

        public int Money { get; private set; }
        private List<Item> _itemsPlayer = new List<Item>();

        public bool GiveMoneyToPay(int priceToPay)
        {
            if (priceToPay <= 0)
            {
                Console.WriteLine("Сумма к оплате введена не верно");
                return false;
            }
            else
            {
                if (Money - priceToPay < 0)
                {
                    Console.WriteLine("Недостаточно монет на счету");
                    return false;
                }
                else
                {
                    Money -= priceToPay;
                    return true;
                }
            }
        }

        public void AddItem(Item itemToAdd)
        {
            _itemsPlayer.Add(itemToAdd);
        }

        public void ShowItems()
        {
            if (_itemsPlayer.Count == 0)
            {
                Console.WriteLine("\nВаш инвентарь пуст");
            }
            else
            {
                Console.WriteLine("\nТвои предметы:\n ");
                for (int i = 0; i < _itemsPlayer.Count; i++)
                {
                    Console.WriteLine($"{i + 1} - {_itemsPlayer[i].Name}, кол-во {_itemsPlayer[i].Count}");
                }
            }
        }
    }

    class Shop
    {
        private bool _isTrade = true;

        Player _player = new Player(1700);

        Trader _trader = new Trader(new List<Item>()
        {
            new Item("Меч", 250, 1),
            new Item("Лук", 500, 1),
            new Item("Стрелы", 5, 100),
            new Item("Броня", 1000, 1),
            new Item("Зелье здоровья", 100, 10)
        });

        private int InputAndCheckToMuch()
        {
            string input = Console.ReadLine();
            int number;

            bool canParce = Int32.TryParse(input, out number);

            while (!canParce || number < 1)
            {
                Console.Write("\nНекорректный ввод, введите целое число больше 0: ");
                input = Console.ReadLine();

                canParce = Int32.TryParse(input, out number);
            }

            return number;
        }

        public void Work()
        {
            while (_isTrade)
            {
                Console.WriteLine("Торговля");

                Console.WriteLine($"\nУ вас {_player.Money} золота");

                Console.WriteLine("\nСписок предметов для покупки:");

                _trader.ShowItems();

                Console.WriteLine("\nМеню:");
                Console.WriteLine("1 - Показать ваши предметы" +
                                  "\n2 - Купить предмет по номеру" +
                                  "\n3 - Покинуть торговца");
                Console.Write("\nВведите номер: ");
                string menuItem = Console.ReadLine();

                while (menuItem != "1" && menuItem != "2" && menuItem != "3")
                {
                    Console.Write("\nТакого пунка меню нет, введите номер ещё раз: ");
                    menuItem = Console.ReadLine();
                }

                switch (menuItem)
                {
                    case "1":

                        _player.ShowItems();
                        break;

                    case "2":

                        Console.Write("\nВведите номер предмета: ");

                        int itemNumber = InputAndCheckToMuch();

                        if (itemNumber < 0 || itemNumber > _trader.ItemsTrader.Count)
                        {
                            Console.WriteLine("\nПредмета под таким номером нет");
                            break;
                        }

                        itemNumber--; //в листе начинается с 0, в списке с 1, вводится номер из списка, поэтому -1

                        Console.Write("\nВведите желаемое количество: ");

                        int countToPay = InputAndCheckToMuch();

                        if (_trader.ItemsTrader[itemNumber].Count < countToPay)
                        {
                            Console.WriteLine("\nНедостаточно предметов или количество введено не корректно");
                            break;
                        }

                        _trader.SellItems(_player, itemNumber, countToPay);
                        
                        break;

                    case "3":
                        _isTrade = false;
                        break;
                }

                Console.WriteLine("\nНажмите любую кнопку чтобы обновить список");
                Console.ReadKey();
                Console.Clear();
            }
        }
    }
}