﻿using System;
using System.Collections.Generic;

    // Задача:
    // Написать программу администрирования супермаркетом.
    // В супермаркете есть очередь клиентов.
    // У каждого клиента в корзине есть товары, также у клиентов есть деньги.
    // Клиент когда подходит на кассу получает итоговую сумму покупки и старается её оплатить.
    // Если оплатить клиент не может, то он рандомный товар из корзины выкидывает до тех пор пока его денег не хватит для оплаты.
    // Клиентов можно делать ограниченное число на старте программы.

namespace SuperMarket
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            SuperMarket superMarket = new SuperMarket();
            superMarket.Start();
        }
        
        public static Random Random = new Random();

        class SuperMarket
        {
            private bool _isWork = true;

            private Client _client;
            
            private Queue<Client> _clientsQueue = new Queue<Client>();

            public void Start()
            {
                Console.WriteLine("Выберите режим работы супермаркета");
                Console.WriteLine("1 - Бесконечная очередь" +
                                  "\n2 - Случайное количество человек в очереди");
                
                Console.Write("Ввод: ");
                string mode = Console.ReadLine();

                while (mode != "1" && mode != "2")
                {
                    Console.Write("\nТакого режима работы нет, введите корректное число: ");
                    mode = Console.ReadLine();
                }
                
                Console.Clear();

                switch (mode)
                {
                    case "1":
                        WorkWithEndlessQueue();
                        break;
                    case "2":
                        WorkWithRandomAmountClients();
                        break;
                }
            }

            private void WorkWithEndlessQueue()
            {
                while (_isWork)
                {
                    Console.WriteLine("Супермаркет");

                    _client = new Client();

                    Console.WriteLine("\nТекущий покупатель:");
                    Console.WriteLine($"\nCумма к оплате {_client.ShowBasketTotalPrice()} рублей");

                    _client.Pay();

                    Console.WriteLine("\nЧтобы начать обслуживание следующего покупателя, нажмите любую кнопку");
                    Console.WriteLine("Чтобы закрыть магазин, нажми Esc");

                    var input = Console.ReadKey().Key;
                    Console.Clear();
                    

                    if (input == ConsoleKey.Escape)
                    {
                        Console.WriteLine("\nСмена закончена, чтобы закрыть магазин, нажми любую кнопку");
                        Console.ReadKey();
                        _isWork = false;
                    }
                }
            }

            private void WorkWithRandomAmountClients()
            {
                void SetRandomClients()
                {
                    int minClients = 5;
                    int maxClients = Random.Next(minClients, 25);

                    for (int i = 0; i < maxClients; i++)
                    {
                        _clientsQueue.Enqueue(new Client());
                    }
                }

                SetRandomClients();

                while (_isWork)
                {
                    Console.WriteLine("Супермаркет");

                    _client = _clientsQueue.Dequeue();

                    Console.WriteLine("\nТекущий покупатель:");
                    Console.WriteLine($"\nCумма к оплате {_client.ShowBasketTotalPrice()} рублей");
                    
                    _client.Pay();

                    Console.WriteLine("\nЧтобы начать обслуживание следующего покупателя, нажмите любую кнопку");
                    Console.ReadKey();
                    Console.Clear();

                    if (_clientsQueue.Count == 0)
                    {
                        Console.WriteLine("\nСмена закончена, чтобы закрыть магазин, нажми любую кнопку");
                        Console.ReadKey();
                        _isWork = false;
                    }
                }
            }
        }

        class Client
        {
            private List<Product> _basket = new List<Product>();
            
            public int Cash { get; private set; }

            private void SetRandomProductsInBasket(int minProducts, int maxProducts)
            {
                maxProducts = Random.Next(minProducts, maxProducts + 1);
                
                for (int i = 0; i < maxProducts; i++)
                {
                    _basket.Add(new Product());
                }
            }

            private void SetRandomCash(int minCash, int maxCash)
            {
                maxCash++;
                Cash = Random.Next(minCash, maxCash);
            }

            public int ShowBasketTotalPrice()
            {
                int totalPrice = 0;

                for (int i = 0; i < _basket.Count; i++)
                {
                    totalPrice += _basket[i].Price;
                }

                return totalPrice;
            }

            public void Pay()
            {
                Console.WriteLine("\nНажмите любую кнопку, чтобы попробывать оплатить товары");
                Console.ReadKey();
                
                if (ShowBasketTotalPrice() > Cash)
                {
                    while (ShowBasketTotalPrice() > Cash)
                    {
                        Console.WriteLine("\nОй, это слишком дорого, я выкладываю один товар");
                        
                        _basket.RemoveAt(_basket.Count - 1);
                        
                        Console.WriteLine($"\nТеперь сумма к оплате {ShowBasketTotalPrice()} рублей");
                        
                        Console.WriteLine("\nНажмите любую кнопку, чтобы попробывать оплатить товары");
                        Console.ReadKey();
                    }
                }
                
                Console.WriteLine("\nЭто как раз то, что нужно, я плачу!");

                Cash -= ShowBasketTotalPrice();
            }

            public Client()
            {
                SetRandomProductsInBasket(minProducts:1, maxProducts:10);
                SetRandomCash(minCash:1000, maxCash:5000);
            }
        }

        class Product
        {
            public int Price { get; private set; }

            private void SetRandomPrice(int minPrice, int maxPrice)
            {
                maxPrice++;
                Price = Random.Next(minPrice, maxPrice);
            }
            
            public Product()
            {
                SetRandomPrice(minPrice:15, maxPrice:1000);
            }
        }
    }
}