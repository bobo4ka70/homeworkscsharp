﻿using System;
using System.Collections.Generic;

    // Задача:
    // Создать 5 бойцов, пользователь выбирает 2 бойцов и они сражаются друг с другом до смерти. У каждого бойца могут быть свои статы.
    // Каждый боец должен иметь особую способность для атаки, которая свойственна только его классу!
    //
    // От Вовы: Войнов я не балансил

namespace GladiatorFights
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Arena arena = new Arena();
            arena.StartFight();
        }
        
        public static Random Random = new Random();

        class Arena
        {
            private bool _isFight = true;
            private List<Warrior> _warriors = new List<Warrior>();
            private Warrior _leftWarrior;
            private Warrior _rightWarrior;

            private void AddWarrirors()
            {
                _warriors.Add(new Barbarian("Варвар", "наскок",2000, 25, 300));
                _warriors.Add(new Amazon("Амазонка", "бросок копья", 2500, 25, 250));
                _warriors.Add(new Hunter("Охотник", "быстрый выстрел из лука", 2500, 15, 275));
                _warriors.Add(new Knight("Рыцарь", "размашистый удар",3000, 50, 150));
                _warriors.Add(new Rogue("Разбойник", "удар в спину", 2000, 10, 400));
            }
            
            private void ShowWarriors()
            {
                for (int i = 0; i < _warriors.Count; i++)
                {
                    Console.Write($"{i + 1} ");
                    _warriors[i].ShowCharacterStats();
                }
            }

            private Warrior TakeFighterToTheArena(int indexWarrior)
            {
                var temp = _warriors[indexWarrior];
                _warriors.RemoveAt(indexWarrior);
                return temp;
            }

            private int InputAndCheckAvabilityInTheList()
            {
                string inputNumber = Console.ReadLine();
                int warriorIndex;

                bool canParse = int.TryParse(inputNumber, out warriorIndex);

                while (canParse == false || warriorIndex - 1 < 0 || warriorIndex > _warriors.Count)
                {
                    Console.Write("\nБойца с таким номером не существует, попробуйте снова: ");
                    inputNumber = Console.ReadLine();
                    
                    canParse = int.TryParse(inputNumber, out warriorIndex);
                }

                return --warriorIndex;
            }
            
            public void StartFight()
            {
                Console.WriteLine("Добро пожаловать на арену!");
                
                AddWarrirors();
                ShowWarriors();

                Console.Write("\nВведите номер левого бойца, которого выведут на арену: ");
                int leftWarrior = InputAndCheckAvabilityInTheList();
                
                _leftWarrior = TakeFighterToTheArena(indexWarrior: leftWarrior);
                
                Console.Clear();
                
                ShowWarriors();
                
                Console.Write("\nВведите номер правого бойца, которого выведут на арену: ");
                int rightWarrior = InputAndCheckAvabilityInTheList();
                
                _rightWarrior = TakeFighterToTheArena(indexWarrior: rightWarrior);
                
                Console.Clear();

                while (_isFight)
                {
                    _leftWarrior.ShowStatus();
                    _rightWarrior.ShowStatus();
                    
                    _leftWarrior.Attack(attacking:_rightWarrior, abilityDamage: _leftWarrior.ApplyUniqueAbility());

                    _rightWarrior.Attack(attacking:_leftWarrior, abilityDamage: _rightWarrior.ApplyUniqueAbility());

                    if (_leftWarrior.Health == 0 || _rightWarrior.Health == 0)
                    {
                        _isFight = false;
                    }

                    else
                    {
                        Console.WriteLine("\nНажмите любую кнопку, чтобы начать следующий раунд");
                        Console.ReadKey();
                        Console.Clear();
                    }
                }

                if (_leftWarrior.Health == 0 && _rightWarrior.Health == 0)
                {
                    Console.WriteLine("\nОба бойца пали в равном бою!");
                }
                else if (_rightWarrior.Health == 0)
                {
                    Console.WriteLine($"\nСтатус победиля получает {_leftWarrior.Name}");
                }
                else if (_leftWarrior.Health == 0)
                {
                    Console.WriteLine($"\nСтатус победиля получает {_rightWarrior.Name}");
                }

                Console.WriteLine("\nНажми любую кнопку, чтобы выйти из программы");
                Console.ReadKey();
                Console.Clear();
            }
        }

        abstract class Warrior
        {
            protected Warrior(string name, string ability, int health, int armor, int damage)
            {
                void ShowErrorAndCloseApplication(string variableName, int comparedValue)
                {
                    Console.WriteLine(
                        $"Ошибка в создании объекта класса Warrior, переменная {variableName} < {comparedValue}");
                    Console.WriteLine("Нажмите любую кнопку, чтобы закрыть программу");
                    Console.ReadKey();
                    Environment.Exit(0);
                }

                if (health < 1)
                {
                    ShowErrorAndCloseApplication("здоровье", 1);
                }
                else if (armor < 0)
                {
                    ShowErrorAndCloseApplication("броня", 0);
                }
                else if (damage < 1)
                {
                    ShowErrorAndCloseApplication("урон", 1);
                }
                else
                {
                    Health = health;
                    Armor = armor;
                    Damage = damage;
                    Name = name;
                    Ability = ability;
                }
            }

            public string Name { get; private set; }
            public string Ability { get; private set; }
            public int Health { get; private set; }
            public int Armor { get; private set; }
            public int Damage { get; private set; }

            public abstract int ApplyUniqueAbility();
            
            private string SelectAttackType(Random random)
            {
                string[] choises = "Standard Attack|Attack With Ability".Split('|');
                return choises[random.Next(choises.Length)];
            }

            public void Attack(Warrior attacking, int abilityDamage)
            {
                void DealDamage()
                {
                    if (Health - (attacking.Damage - Armor) <= 0)
                    {
                        Health = 0;
                    }
                    else
                    {
                        Health -= (attacking.Damage - Armor);
                    }
                }

                void DealDamageWithAbility()
                {
                    if (Health - (abilityDamage - Armor) <= 0)
                    {
                        Health = 0;
                    }
                    else
                    {
                        Health -= (abilityDamage - Armor);
                    }
                }

                switch (SelectAttackType(Random))
                {
                    case "Standard Attack":
                        DealDamage();
                        Console.WriteLine($"{Name} наносит {Damage} урона простым ударом!");
                        break;
                    case "Attack With Ability":
                        DealDamageWithAbility();
                        Console.WriteLine($"{Name} использует {Ability} и наносит {ApplyUniqueAbility()} урона!");
                        break;
                }
            }
            
            public void ShowCharacterStats()
            {
                Console.WriteLine(
                    $"{Name} HP - {Health}, armor - {Armor}, damage {Damage}, способность - {Ability}");
            }

            public void ShowStatus()
            {
                Console.WriteLine($"{Name} HP - {Health}");
            }
        }

        class Barbarian : Warrior
        {
            public override int ApplyUniqueAbility()
            {
                return Damage + 150;
            }

            public Barbarian(string name, string ability, int health, int armor, int damage) : base(name, ability, health, armor, damage)
            {
            }
        }

        class Amazon : Warrior
        {
            public override int ApplyUniqueAbility()
            {
                return Damage + 200;
            }

            public Amazon(string name, string ability, int health, int armor, int damage) : base(name, ability, health, armor, damage)
            {
            }
        }

        class Hunter : Warrior
        {
            public override int ApplyUniqueAbility()
            {
                return Damage + 175;
            }

            public Hunter(string name, string ability, int health, int armor, int damage) : base(name, ability, health, armor, damage)
            {
            }
        }

        class Rogue : Warrior
        {
            public override int ApplyUniqueAbility()
            {
                return Damage + 300;
            }

            public Rogue(string name, string ability, int health, int armor, int damage) : base(name, ability, health, armor, damage)
            {
            }
        }

        class Knight : Warrior
        {
            public override int ApplyUniqueAbility()
            {
                return Damage + 225;
            }

            public Knight(string name, string ability, int health, int armor, int damage) : base(name, ability, health, armor, damage)
            {
            }
        }
    }
}